#include <iostream>
#include <cmath>

using namespace std;

float minor(int n, float** m)
{
    int count=0;
    for (int i=0;i<n;i++)
    {
        float max=m[i][i];
        int maxnum=i;
        for (int j=i;j<n;j++) // поиск максимума в столбце
        {
            if ((m[j][i]>max && m[j][i] != 0) || (m[j][i] != 0 && max == 0))
            {
                max=m[j][i];
                maxnum=j;
            }
        }
        if (max == 0) // если на диагонале нулевой элемент
        {
            return 0;
        }
        if (maxnum != i) // перестановка максимума на диагональ
        {
            count++;
            for (int j=0;j<n;j++)
            {
                float temp = m[i][j];
                m[i][j] = m[maxnum][j];
                m[maxnum][j] = temp;
            }
        }
        for (int j=i+1;j<n;j++) // прямой ход
        {
            float v=m[j][i]/m[i][i]; // m21/m11
            for (int k=0;k<n;k++)
            {
                m[j][k]=m[j][k]-m[i][k]*v;
            }
        }
    }
    float det=1;
    for (int i=0;i<n;i++)
    {
        det=det*m[i][i];
    }
    return det*pow(-1,count);
}

int main()
{
    int n; // размерность матриц
    int count=0; // количество перестановок строк для знака определителя
    float** A; // AX=B
    float** AT; // транспонированная матрица
    float* B;
    float* X;
    cout<<"Введите размер матриц"<< endl;
    cin>>n;
    A=new float* [n]; // строки
    AT=new float* [n]; // столбцы
    B=new float[n];
    X=new float[n];
    cout<<"Введите элементы матрицы А построчно через пробел"<< endl;
    for (int i=0; i<n; i++)
    {
        A[i]=new float[n]; // столбцы
        AT[i]=new float[n]; // строки
        for (int j=0;j<n;j++)
        {
            cin>>A[i][j];
            AT[i][j]=A[i][j];
        }
    }
    cout<<"Введите элементы матрицы B через пробел"<< endl;
    for (int i=0; i<n; i++)
    {
        cin>>B[i];
    }
    // поиск максимума по столбцу и прямой ход
    for (int i=0;i<n;i++)
    {
        float max=A[i][i];
        int maxnum=i;
        for (int j=i;j<n;j++) // поиск максимума в столбце
        {
            if ((A[j][i]>max && A[j][i] != 0) || (A[j][i] != 0 && max == 0))
            {
                max=A[j][i];
                maxnum=j;
            }
        }
        if (max == 0) // если на диагонале нулевой элемент
        {
            cout << "нет решения\n";
            return 0;
        }
        if (maxnum != i) // перестановка максимума на диагональ
        {
            count++;
            float temp=B[i];
            B[i]=B[maxnum];
            B[maxnum]=temp;
            for (int j=0;j<n;j++)
            {
                temp = A[i][j];
                A[i][j] = A[maxnum][j];
                A[maxnum][j] = temp;
            }
        }
        for (int j=i+1;j<n;j++) // прямой ход
        {
            float m=A[j][i]/A[i][i]; // a21/a11
            B[j]=B[j]-B[i]*m;
            for (int k=0;k<n;k++)
            {
                A[j][k]=A[j][k]-A[i][k]*m;
            }
        }
    }
    // обратный ход
    for (int i=n-1;i>=0;i--)
    {
        float sum=0;
        for (int j=n-1;j>i;j--)
        {
            sum=sum+A[i][j]*X[j];
        }
        X[i]=(B[i]-sum)/A[i][i];
    }
    // вывод результата
    cout<<"X=[ ";
    for (int i=0;i<n;i++)
    {
        cout<<X[i]<< " ";
    }
    cout<<"]"<<endl;

    // расчет обратной матрицы
    float detA = 1; // определитель матрицы А
    for (int i=0;i<n;i++)
    {
        detA=detA*A[i][i];
    }
    float** A1 = new float* [n]; // обратная матрица
    for (int i=0;i<n;i++)
    {
        A1[i] = new float[n];
        for (int j=0;j<n;j++)
            A1[i][j] = 0;
    }
    for (int j=0;j<n;j++)
    {
        for (int i=0;i<n;i++)
        {
            float** mm = new float* [n-1]; // для расчета миноров
            for (int k=0;k<n-1;k++)
            {
                mm[k] = new float [n-1];
                for (int l=0;l<n-1;l++)
                {
                    if (k>=j && l>=i)
                        mm[k][l] = AT[l+1][k+1];
                    else if (k>=j && l<i)
                        mm[k][l] = AT[l][k+1];
                    else if (k<j && l>=i)
                        mm[k][l] = AT[l+1][k];
                    else if (k<j && l<i)
                        mm[k][l] = AT[l][k];
                }
            }
            A1[j][i] = (minor(n-1,mm)*pow(-1,i+j))/detA;
        }
    }
    // вывод результатов
    cout<<"A1=\n";
    for (int i=0;i<n;i++)
    {
        for (int j=0;j<n;j++)
        {
            cout<<A1[i][j]<<" ";
        }
        cout<<"\n";
    }
    return 0;
}